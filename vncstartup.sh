#!/bin/bash
/bin/bash -c "/usr/bin/websockify -D --web=/usr/share/novnc/ --cert=/etc/pki/tls/certs/novnc.pem 6080 localhost:5901 &"
/bin/bash -c "/usr/bin/vncserver :1 -geometry 1200x1024 -depth 24 &"
